import { Note, getNotes } from "../models";

const indexAction = async (req, res) => res.send(await getNotes());

const addNoteAction = async (req, res) => {
    const title = req.body.title;
    if(title) {
        Note.create({
            title
        })
            .then(note => {
                res.statusCode = 200;
                res.send(note);
            })
            .catch(_ => {
                res.statusCode = 500;
                res.send({})
            });
    }else {
        res.statusCode = 500;
        res.send({});
    }
}

const updateNoteAction = async (req, res) => {
    const id = req.body.id;
    const title = req.body.title;
    if(id && title) {
        Note.updateOne({
            _id: id
        }, {title})
            .then(note => {
                res.statusCode = 200;
                res.send(note);
            })
            .catch(_ => {
                res.statusCode = 500;
                res.send({});
            })
    } else {
        res.statusCode = 500;
        res.send({});
    }
}

const removeNoteAction = async (req, res) => {
    console.log(req.body);
    const id = req.body.id;
    Note.deleteOne({ _id: id }, (err) => {
        if(err) {
            res.statusCode = 500;
            res.send({});
        }else {
            res.statusCode = 200;
            res.send({ ok: 1 });
        }
    })
}

export { indexAction, addNoteAction, updateNoteAction, removeNoteAction };
