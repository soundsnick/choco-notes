import bodyParser from 'body-parser';
import express from 'express';
import { addNoteAction, indexAction, removeNoteAction, updateNoteAction } from "./controllers/noteController";
import cors from "cors";

const app = express();

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", async (request, response) => {
    await indexAction(request, response);
});

app.post("/note/add", async (request, response) => {
    await addNoteAction(request, response);
});

app.delete("/note/remove", async (request, response) => {
    await removeNoteAction(request, response);
})

app.put("/note/update", async (request, response) => {
    await updateNoteAction(request, response);
})

app.listen(3000);
