import mongoose from "mongoose";
import { database } from "../core/database";
const Schema = mongoose.Schema;

const schema = new Schema(
    {
        title: {
            type: String,
            required: true
        }
    },
    {
        timestamps: true
    }
);


const Note = database.model('Note', schema);
const getNotes = async () => await Note
    .find({})
    .sort({id: -1});

export { Note, getNotes };