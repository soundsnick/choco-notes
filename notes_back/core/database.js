import mongoose from "mongoose";

const database = mongoose.createConnection('mongodb://database:27017/notes', { useNewUrlParser: true });

export { database };