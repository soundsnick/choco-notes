import { api } from "@/api";

const actions = {
    getNotes({ commit, dispatch }) {
        api.get('/')
            .then(data => {
                console.log(data.data);
                commit('setNotes', data.data);
            })
            .catch(err => {
                console.error(err);
                dispatch('throwMessage', { type: "error", text: "Server error! Try again later" });
            })
    },
    addNote({ state, commit, dispatch }, payload) {
        if(state.notes.length < 10){
            api.post('/note/add', { title: payload })
                .then(data => {
                    commit('addNote', data.data);
                    commit('setFormLoading', false);
                    dispatch('throwMessage', { type: "success", text: "Note created successfully!" });
                })
                .catch(err => {
                    console.error(err);
                    commit('setFormLoading', false);
                    dispatch('throwMessage', { type: "error", text: "Server error! Try again later" });
                });
        }else {
            setTimeout(() => commit('setFormLoading', false), 300);
            dispatch('throwMessage', { type: "error", text: "You've reached the maximum(10) amount of notes. Delete one to add another." });
        }
    },
    updateNote({ dispatch }, payload){
        api.put('/note/update', { id: payload.id, title: payload.text })
            .then(() => dispatch('throwMessage', { type: "success", text: "Note updated successfully!" }))
            .catch(err => {
                console.error(err);
                dispatch('throwMessage', { type: "error", text: "Something went wrong." })
            })
    },
    deleteNote({ commit, dispatch }, payload){
        api.delete('/note/remove',  { data: { id: payload.id }})
            .then(() => {
                commit('updateNotes', payload.id);
                dispatch('throwMessage', { type: "success", text: "Note deleted successfully!" });
            })
            .catch(err => {
                console.error(err);
                dispatch('throwMessage', { type: "error", text: "Something went wrong." })
            })
    },
    throwMessage({ commit }, payload) {
        commit('setMessage', payload);
        setTimeout(() => {
            commit('setMessage', null);
        }, 5000);
    }
};

export { actions };