const mutations = {
    setNotes(state, payload) {
        state.notes = payload;
    },
    updateNotes(state, payload) {
        state.notes = state.notes.filter(n => n._id !== payload);
    },
    addNote(state, payload) {
        state.notes.unshift(payload);
    },
    setMessage(state, payload) {
        state.message = payload;
    },
    setFormLoading(state, payload) {
        state.formLoading = payload;
    }
};

export { mutations };