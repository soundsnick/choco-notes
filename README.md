# Installation
Requirements:
* Docker
* Docker-compose

## Building and running
Just use docker-compose
```bash
$ docker-compose up -d
```

### Profit
Go to `localhost:8080` and check it.